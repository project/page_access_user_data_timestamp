INTRODUCTION
------------

# Page Access User Data Timestamp

## Description
This module writes the current timestamp value to the users_data table everytime hook_preprocess_page() is called.
This is a utility module intended for use with other modules.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/page_access_user_data_timestamp

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/page_access_user_data_timestamp


REQUIREMENTS
------------

This module requires the following modules:

 * User


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
1. **Install** this module


MAINTAINERS
-----------

Current maintainers:
 * Preston Schmidt - https://www.drupal.org/user/3594865
